<?php
/*****************
1ère NSI
processus 
******************/
global $pages;

// Bases du javascript
$menu_activite = array(
    "titre" => "Processus",
    "contenu" => [
        'introduction',
        'creation_thread',
        'interblocage'
    ]
);

// pages des bases du javascript
$processus_pages = array(
    'introduction' => array(
        "template" => 'processus/introduction.twig.html',
        "menu" => 'processus',
        'page_precedente' => NULL,
        'page_suivante' => 'creation_thread',
        'titre' => 'Introduction',
        "css" => 'ressources/processus/assets/css/processus.css'
    ),
    'creation_thread' => array(
        "template" => 'processus/creation_thread.twig.html',
        "menu" => 'processus',
        'page_precedente' => 'introduction',
        'page_suivante' => 'interblocage',
        'titre' => 'Création de thread',
        "css" => 'ressources/processus/assets/css/processus.css'
    ),
    'interblocage' => array(
        "template" => 'processus/interblocage.twig.html',
        "menu" => 'processus',
        'page_precedente' => 'creation_thread',
        'page_suivante' => NULL,
        'titre' => 'Interblocage',
        "css" => 'ressources/processus/assets/css/processus.css'
    ),
);

$pages = array_merge($pages, $processus_pages);
