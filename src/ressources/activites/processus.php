<?php

register_activity(
    'processus',
    array(
        'category' => 'NSIT',
        'section' => 'NSIThardos',
        'type' => 'url',
        'titre' => "Processus",
        'auteur' => "Laurent COOPER",
        'URL' => 'index.php?activite=processus&page=introduction',
        'commentaire' => "Alors que nos ordinateurs n'ont qu'un seul processeur, comment font ils pour éxécutez simultanément de multiples tâches ?",
        'directory' => 'processus',
        'icon' => 'fas fa-cogs',
        'prerequis' => NULL
    )
);
